#!/bin/bash
# Format pel nom de fitxer, basat en la data dels backups

FORMAT=$(date+"%Y-%m-%d_%H-%M-%S")

# Directori del qual volem un backup,
# passat com a paràmetre través de la línea de comandes

SOURCE="$1"

# Destinació del backup

DESTINATION="/backups"

# Crear el fitxer amb el format esmentat

FILE="$DESTINATION/backup_$FORMAT.tar.gz"

# Creació del fitxer amb la comanda tar

tar -czvf $FILE -R $SOURCE

# Comprovació d'errors

if [$? -eq 0]; then 
  echo "Backup Succesful"
 else
   echo "Backup Failed"
 fi
